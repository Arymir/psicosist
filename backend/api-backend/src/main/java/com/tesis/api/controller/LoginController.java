package com.tesis.api.controller;

import com.tesis.api.dto.TokenDto;
import com.tesis.api.dto.UserDto;
import com.tesis.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<Mono<UserDto>> login(@RequestBody TokenDto token) throws Exception {
        Mono<UserDto> userDto = userService.getUserFromFacebook(token);
        return new ResponseEntity(userDto, HttpStatus.OK);
    }
}
