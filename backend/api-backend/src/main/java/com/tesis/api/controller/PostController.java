package com.tesis.api.controller;

import com.tesis.api.dto.PostDto;
import com.tesis.api.dto.ResultadoAnalisDto;
import com.tesis.api.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/post")
public class PostController {

    @Autowired
    private PostService postService;

    @GetMapping("/findByUser/{userId}")
    public ResponseEntity<Flux<PostDto>> login(@PathVariable String userId) throws Exception {
        Flux<PostDto> listPost = postService.findByUserId(userId);
        return new ResponseEntity(listPost, HttpStatus.OK);
    }

    @GetMapping("/analisis/{userId}")
    public ResponseEntity<Mono<ResultadoAnalisDto>> analisis(@PathVariable String userId) throws Exception {
        Mono<ResultadoAnalisDto> response = postService.ejecutarAnalisis(userId);
        return new ResponseEntity(response, HttpStatus.OK);
    }
}
