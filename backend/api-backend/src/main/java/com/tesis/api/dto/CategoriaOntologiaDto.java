package com.tesis.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CategoriaOntologiaDto {
    private String nombre;
    private List<String> listaValores;

    public CategoriaOntologiaDto(@JsonProperty("categoria") String nombre, @JsonProperty("valores") List<String> valores){
        this.nombre = nombre;
        this.listaValores = valores;
    }
}
