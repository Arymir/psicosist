package com.tesis.api.dto;

import lombok.Builder;
import lombok.Data;
import java.time.LocalDateTime;

@Data
@Builder
public class PostDto {
    private String id;
    private String text;
    private LocalDateTime createdDateTime;
}
