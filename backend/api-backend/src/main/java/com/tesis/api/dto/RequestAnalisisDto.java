package com.tesis.api.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class RequestAnalisisDto {
    private List<PostDto> listaPost;
}
