package com.tesis.api.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseAnalisDto {
    private final List<TokenCountDto> listaTokenCounts;
    private final List<CategoriaOntologiaDto> listaRelacionOntologia;
    private final String nubePalabasBase64;
    private final float probabilidad;

    @JsonCreator
    public ResponseAnalisDto(@JsonProperty("listaTokenCounts") List<TokenCountDto> listaTokenCounts,
                             @JsonProperty("relacionOntologia") List<CategoriaOntologiaDto> listaRelacionOntologia,
                             @JsonProperty("nubePalabrasBase64") String nubePalabasBase64,
                             @JsonProperty("probabilidad") float probabilidad) {
        this.listaTokenCounts = listaTokenCounts;
        this.listaRelacionOntologia = listaRelacionOntologia;
        this.nubePalabasBase64 = nubePalabasBase64;
        this.probabilidad = probabilidad;
    }
}
