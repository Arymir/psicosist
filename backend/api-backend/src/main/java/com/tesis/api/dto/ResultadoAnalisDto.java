package com.tesis.api.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class ResultadoAnalisDto {
    private List<TokenCountDto> listaTokenCounts;
    private List<CategoriaOntologiaDto> listaRelacionOntologia;
    private String nubePalabrasBase64;
    private String presenciaSintomas;
    private String nivelDepresion;
    private String recomendacion;
    private float probabilidad;
    private LocalDate fechaGeneracion;
}
