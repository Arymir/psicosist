package com.tesis.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenCountDto {
    private final String text;
    private final Integer value;

    public TokenCountDto(@JsonProperty("token") String token, @JsonProperty("count") Integer count){
        this.text = token;
        this.value = count;
    }
}
