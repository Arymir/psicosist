package com.tesis.api.dto;

import lombok.Data;

@Data
public class TokenDto {
    private String value;
}
