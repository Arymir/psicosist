package com.tesis.api.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class UserDto {
    private String id;
    private String email;
    private String name;
    private String gender;
    private String avatarUrl;
    private LocalDateTime createdDateTime;
    private LocalDateTime updatedDateTime;
}
