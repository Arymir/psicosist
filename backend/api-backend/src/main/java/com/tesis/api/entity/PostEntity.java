package com.tesis.api.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@Document(collection = "posts")
public class PostEntity {
    private String id;
    private String userId;
    private String message;
    private LocalDateTime createdDateTime;
}
