package com.tesis.api.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Builder
@Document(collection = "users")
public class UserEntity {
    @Id
    private String id;
    private String email;
    private String name;
    private String gender;
    private String avatarUrl;
    private LocalDateTime createdDateTime;
    private LocalDateTime updatedDateTime;
}
