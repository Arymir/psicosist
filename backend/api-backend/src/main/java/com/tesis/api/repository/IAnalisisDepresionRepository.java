package com.tesis.api.repository;

import com.tesis.api.dto.PostDto;
import com.tesis.api.dto.ResponseAnalisDto;

import java.util.List;

public interface IAnalisisDepresionRepository {

    ResponseAnalisDto obtenerResultadoAnalisisDepresion(List<PostDto> listaPost);
}
