package com.tesis.api.repository;

import com.tesis.api.entity.PostEntity;
import reactor.core.publisher.Flux;

public interface ICustomPostRepository {
    Flux<PostEntity> findByUserId(String id, int modoOrdenamiento);
}
