package com.tesis.api.repository;

import com.tesis.api.entity.PostEntity;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPostRepository extends ReactiveMongoRepository<PostEntity, String> {
}
