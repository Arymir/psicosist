package com.tesis.api.repository.impl;

import com.tesis.api.dto.PostDto;
import com.tesis.api.dto.RequestAnalisisDto;
import com.tesis.api.dto.ResponseAnalisDto;
import com.tesis.api.repository.IAnalisisDepresionRepository;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Repository;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import java.time.Duration;
import java.util.List;

@Repository
public class AnalisisDepresionRepositoryImpl implements IAnalisisDepresionRepository {
    @Override
    public ResponseAnalisDto obtenerResultadoAnalisisDepresion(List<PostDto> listaPost) {
        RequestAnalisisDto requestAnalisisDto = RequestAnalisisDto.builder()
                .listaPost(listaPost)
                .build();

        HttpClient httpClient = HttpClient.create()
                .responseTimeout(Duration.ofSeconds(3600));

        return WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .build().post().uri("http://localhost:9091/analisisDepresion")
                .body(BodyInserters.fromValue(requestAnalisisDto))
                .retrieve()
                .bodyToMono(ResponseAnalisDto.class).block();
    }
}
