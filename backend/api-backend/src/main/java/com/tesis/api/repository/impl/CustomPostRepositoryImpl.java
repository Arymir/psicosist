package com.tesis.api.repository.impl;

import com.tesis.api.entity.PostEntity;
import com.tesis.api.repository.ICustomPostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public class CustomPostRepositoryImpl implements ICustomPostRepository {

    private final ReactiveMongoTemplate mongoTemplate;

    @Autowired
    public CustomPostRepositoryImpl(ReactiveMongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Flux<PostEntity> findByUserId(String userId, int modoOrdenamiento) {
        Sort.Direction orden = modoOrdenamiento < 0 ? Sort.Direction.DESC: Sort.Direction.ASC;
        Query query = new Query(Criteria.where("userId").is(userId)).with(Sort.by(orden, "id"));
        return mongoTemplate.find(query, PostEntity.class);
    }
}
