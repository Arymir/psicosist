package com.tesis.api.service;

import com.tesis.api.dto.PostDto;
import com.tesis.api.dto.ResponseAnalisDto;
import com.tesis.api.dto.ResultadoAnalisDto;
import com.tesis.api.repository.IAnalisisDepresionRepository;
import com.tesis.api.repository.ICustomPostRepository;
import com.tesis.api.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.stream.Collectors;

@Service
public class PostService {
    @Autowired
    private ICustomPostRepository postRepository;

    @Autowired
    private IAnalisisDepresionRepository analisisDepresionRepository;

    public Flux<PostDto> findByUserId(String userId){
        return postRepository.findByUserId(userId, -1)
                .map(Mapper::postEntityToPostDto);
    }

    public Mono<ResultadoAnalisDto> ejecutarAnalisis(String userId){
        return postRepository.findByUserId(userId, 1)
                .map(p -> Mapper.postEntityToPostDto(p))
                .collect(Collectors.toList())
                .map(p -> analisisDepresionRepository.obtenerResultadoAnalisisDepresion(p))
                .map(r -> construirResultado(r));
    }

    private ResultadoAnalisDto construirResultado(ResponseAnalisDto responseAnalisDto){
        return ResultadoAnalisDto.builder()
                .listaRelacionOntologia(responseAnalisDto.getListaRelacionOntologia())
                .listaTokenCounts(responseAnalisDto.getListaTokenCounts())
                .probabilidad(responseAnalisDto.getProbabilidad())
                .nubePalabrasBase64(responseAnalisDto.getNubePalabasBase64())
                .presenciaSintomas(obtenerCampoPresenciaSintomas(responseAnalisDto))
                .nivelDepresion(obtenerNivelDepresion(responseAnalisDto))
                .fechaGeneracion(LocalDate.now())
                .recomendacion(obtenerRecomendacion(responseAnalisDto)).build();
    }

    private String obtenerRecomendacion(ResponseAnalisDto responseAnalisDto){
        if(!"NINGUNO".equals(obtenerNivelDepresion(responseAnalisDto))){
           return "SE RECOMIENDO VISITAR AL ESPECIALISTA DE LA SALUD MENTAL";
        }

        return "NINGUNA";
    }

    private String obtenerNivelDepresion(ResponseAnalisDto responseAnalisDto){
        if(responseAnalisDto.getProbabilidad() >= 0.1 && responseAnalisDto.getProbabilidad() <= 0.4){
            return "BAJO";
        }

        if(responseAnalisDto.getProbabilidad() >= 0.5 && responseAnalisDto.getProbabilidad() <= 0.7){
            return "MEDIO";
        }

        if(responseAnalisDto.getProbabilidad() >= 0.8){
            return "ALTO";
        }

        return "NINGUNO";
    }

    private String obtenerCampoPresenciaSintomas(ResponseAnalisDto responseAnalisDto){
        if(!responseAnalisDto.getListaRelacionOntologia().isEmpty()){
            return "SI";
        }

        return "NO";
    }
}
