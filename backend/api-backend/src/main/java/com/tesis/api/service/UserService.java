package com.tesis.api.service;

import com.tesis.api.dto.TokenDto;
import com.tesis.api.dto.UserDto;

import com.tesis.api.entity.PostEntity;
import com.tesis.api.entity.UserEntity;
import com.tesis.api.repository.IPostRepository;
import com.tesis.api.repository.IUserRepository;
import com.tesis.api.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.facebook.api.Facebook;

import org.springframework.social.facebook.api.Post;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IPostRepository postRepository;

    public Flux<UserDto> findAll(){
        return userRepository.findAll().map(Mapper::entityToUserDto);
    }

    public Mono<UserDto> findById(String id){
        return userRepository.findById(id).map(Mapper::entityToUserDto);
    }

    public Mono<UserDto> getUserFromFacebook(TokenDto token) throws Exception {
        try{
            Facebook facebook = new FacebookTemplate(token.getValue());

            final String [] fields = {"id", "email", "picture", "name", "gender"};
            org.springframework.social.facebook.api.User userFacebook = facebook.fetchObject("me", org.springframework.social.facebook.api.User.class, fields);

            return userRepository.findById(userFacebook.getId())
                    .flatMap(updateUser::apply)
                    .switchIfEmpty(insertUser.apply(Mapper.userFacebookToEntity(userFacebook)))
                    .flatMap(userEntity -> insertPosts.apply(userEntity, facebook));
        }catch(Exception ex){
            throw new Exception("Problemas al obtener informacion de facebook");
        }
    }

    private Function<UserEntity, Mono<UserEntity>> updateUser = (userEntity) -> {
        userEntity.setUpdatedDateTime(LocalDateTime.now());
        String urlAvatar = String.format("https://graph.facebook.com/%s/picture?type=small", userEntity.getId());
        userEntity.setAvatarUrl(urlAvatar);
        return userRepository.save(userEntity);
    };

    private Function <UserEntity, Mono<UserEntity>> insertUser = (userEntity) -> {
        LocalDateTime dateTimeCurrent = LocalDateTime.now();
        String urlAvatar = String.format("https://graph.facebook.com/%s/picture?type=small", userEntity.getId());
        userEntity.setAvatarUrl(urlAvatar);
        userEntity.setUpdatedDateTime(dateTimeCurrent);
        userEntity.setCreatedDateTime(dateTimeCurrent);

        return userRepository.insert(userEntity);
    };

    private BiFunction<UserEntity, Facebook, Mono<UserDto>> insertPosts = (userEntity, facebook) -> {
        List<PostEntity> listaPosts = obtenerPostEntity(facebook, userEntity.getId());
        postRepository.insert(listaPosts).subscribe();

        return Mono.just(Mapper.entityToUserDto(userEntity));
    };

    private Predicate<Post> esPostValido = (postFacebook) -> {
        boolean esPublico = postFacebook.getPrivacy().getValue().equals(Post.PrivacyType.EVERYONE);
        boolean esTexto = postFacebook.getType().equals(Post.PostType.STATUS);
        return esPublico && esTexto;
    };

    private List<PostEntity> obtenerPostEntity(Facebook facebook, String userId){
        return facebook.feedOperations().getPosts().stream()
                .filter(esPostValido)
                .map(postFacebook -> Mapper.postFacebookToEntity(postFacebook, userId))
                .collect(Collectors.toList());
    }
}
