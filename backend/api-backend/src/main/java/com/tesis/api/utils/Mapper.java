package com.tesis.api.utils;

import com.tesis.api.dto.PostDto;
import com.tesis.api.dto.UserDto;
import com.tesis.api.entity.PostEntity;
import com.tesis.api.entity.UserEntity;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class Mapper {

    public static UserDto entityToUserDto(UserEntity userEntity){
        return UserDto.builder()
                .id(userEntity.getId())
                .email(userEntity.getEmail())
                .name(userEntity.getName())
                .gender(userEntity.getGender())
                .avatarUrl(userEntity.getAvatarUrl())
                .createdDateTime(userEntity.getCreatedDateTime())
                .updatedDateTime(userEntity.getUpdatedDateTime()).build();
    }

    public static UserEntity userDtoToEntity(UserDto userDto){
        return UserEntity.builder()
                .id(userDto.getId())
                .email(userDto.getEmail())
                .name(userDto.getName())
                .gender(userDto.getGender())
                .avatarUrl(userDto.getAvatarUrl())
                .createdDateTime(userDto.getCreatedDateTime())
                .updatedDateTime(userDto.getUpdatedDateTime()).build();
    }

    public static UserEntity userFacebookToEntity(org.springframework.social.facebook.api.User userFacebook){
        return UserEntity.builder()
                .id(userFacebook.getId())
                .email(userFacebook.getEmail())
                .gender(userFacebook.getGender())
                .name(userFacebook.getName()).build();
    }

    public static PostEntity postFacebookToEntity(org.springframework.social.facebook.api.Post postFacebook, String userId){
        return PostEntity.builder()
                .id(postFacebook.getId())
                .userId(userId)
                .message(postFacebook.getMessage())
                .createdDateTime(dateToLocalDateTime(postFacebook.getCreatedTime()))
                .build();

    }

    public static PostDto postEntityToPostDto(PostEntity postEntity){
        return PostDto.builder()
                .id(postEntity.getId())
                .text(postEntity.getMessage())
                .createdDateTime(postEntity.getCreatedDateTime())
                .build();

    }

    public static LocalDateTime dateToLocalDateTime(Date date){
        return LocalDateTime.ofInstant(
                date.toInstant(), ZoneId.systemDefault());
    }
}
