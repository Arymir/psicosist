import unicodedata as uni
import io
import base64
import pandas as pd
import matplotlib.pyplot as plt
import es_core_news_sm
import pickle
from flask import jsonify
from wordcloud import WordCloud

from vectorizador import Vectorizador

# Librerias para construir el modelo
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report, f1_score, accuracy_score, confusion_matrix
from sklearn.metrics import roc_curve, auc, roc_auc_score

# Bolsas de palabras
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer

# Librerias para la incrustacion de palabras
import gensim 
from gensim.models import Word2Vec

# Librerias para el procesamiento de texto
import re
import nltk  
from nltk.corpus import stopwords
nltk.download('punkt')
nltk.download('stopwords')

class AnalizadorDepresion():

    listaPalabrasVacias = list()
    ontologiaObject = {}

    def __init__(self, idioma, ontologia):
        self.listaPalabrasVacias = stopwords.words(idioma)
        self.nlp = es_core_news_sm.load()        
        self.ontologiaObject = ontologia

    def entrenarNaiveBayesTfIdf(self):         
        df_entrenamiento= self.__obtenerDatosEntrenamiento()
        
        # Creacion del modelo
        model = Word2Vec(df_entrenamiento['clean_text_tok'], min_count=1)
        w2v = dict(zip(model.wv.index2word, model.wv.vectors))

        X_entrenamiento, X_val, y_entrenamiento, y_val = train_test_split(df_entrenamiento["clean_text"],
                                                        df_entrenamiento["target"],
                                                        test_size=0.8,
                                                        shuffle=True)

        X_entrenamiento_tok= [nltk.word_tokenize(i) for i in X_entrenamiento]  
        X_val_tok= [nltk.word_tokenize(i) for i in X_val]      

        tfidf_vectorizer = TfidfVectorizer(use_idf=True)
        X_entrenamiento_vectors_tfidf = tfidf_vectorizer.fit_transform(X_entrenamiento) 
        X_val_vectors_tfidf = tfidf_vectorizer.transform(X_val) 

        modelw = Vectorizador(w2v)
        X_entrenamiento_vectors_w2v = modelw.transformar(X_entrenamiento_tok)
        X_val_vectors_w2v = modelw.transformar(X_val_tok)


        nb_tfidf = MultinomialNB()
        nb_tfidf.fit(X_entrenamiento_vectors_tfidf, y_entrenamiento)

        y_predict = nb_tfidf.predict(X_val_vectors_tfidf)
        y_prob = nb_tfidf.predict_proba(X_val_vectors_tfidf)[:,1]
        
        print(classification_report(y_val, y_predict))
        print('Confusion Matrix:',confusion_matrix(y_val, y_predict))
        
        fpr, tpr, thresholds = roc_curve(y_val, y_prob)
        roc_auc = auc(fpr, tpr)
        print('AUC:', roc_auc)  

        return roc_auc

    
    def entrenarLogicRegressionW2v(self):         
        df_entrenamiento=pd.read_csv('train.csv')
        df_entrenamiento.head()
        
        df_entrenamiento['clean_text'] = df_entrenamiento['text'].apply(lambda x: self.__procesarTexto(x))
        
        df_entrenamiento['clean_text_tok']=[nltk.word_tokenize(i) for i in df_entrenamiento['clean_text']] 
        
        # Creacion del modelo
        model = Word2Vec(df_entrenamiento['clean_text_tok'], min_count=1)
        w2v = dict(zip(model.wv.index2word, model.wv.vectors))

        X_entrenamiento, X_val, y_entrenamiento, y_val = train_test_split(df_entrenamiento["clean_text"],
                                                        df_entrenamiento["target"],
                                                        test_size=0.8,
                                                        shuffle=True)

        X_entrenamiento_tok= [nltk.word_tokenize(i) for i in X_entrenamiento]  
        X_val_tok= [nltk.word_tokenize(i) for i in X_val]      

        tfidf_vectorizer = TfidfVectorizer(use_idf=True)
        X_entrenamiento_vectors_tfidf = tfidf_vectorizer.fit_transform(X_entrenamiento) 
        X_val_vectors_tfidf = tfidf_vectorizer.transform(X_val) 

        modelw = Vectorizador(w2v)
        X_entrenamiento_vectors_w2v = modelw.transformar(X_entrenamiento_tok)
        X_val_vectors_w2v = modelw.transformar(X_val_tok)

        lr_w2v=LogisticRegression(solver = 'liblinear', C=10, penalty = 'l2')
        lr_w2v.fit(X_entrenamiento_vectors_w2v, y_entrenamiento) 

        y_predict = lr_w2v.predict(X_val_vectors_w2v)
        y_prob = lr_w2v.predict_proba(X_val_vectors_w2v)[:,1]
        
        print(classification_report(y_val,y_predict))
        print('Confusion Matrix:',confusion_matrix(y_val, y_predict))
        
        fpr, tpr, thresholds = roc_curve(y_val, y_prob)
        roc_auc = auc(fpr, tpr)
        print('AUC:', roc_auc)  

        return roc_auc
    
    
    def entrenarLogicRegressionTfIdf(self):         
        df_entrenamiento=pd.read_csv('train.csv')
        df_entrenamiento.head()
        
        df_entrenamiento['clean_text'] = df_entrenamiento['text'].apply(lambda x: self.__procesarTexto(x))
        
        df_entrenamiento['clean_text_tok']=[nltk.word_tokenize(i) for i in df_entrenamiento['clean_text']] 
        
        # Creacion del modelo
        model = Word2Vec(df_entrenamiento['clean_text_tok'], min_count=1)
        w2v = dict(zip(model.wv.index2word, model.wv.vectors))

        X_entrenamiento, X_val, y_entrenamiento, y_val = train_test_split(df_entrenamiento["clean_text"],
                                                        df_entrenamiento["target"],
                                                        test_size=0.8,
                                                        shuffle=True)

        X_entrenamiento_tok= [nltk.word_tokenize(i) for i in X_entrenamiento]  
        X_val_tok= [nltk.word_tokenize(i) for i in X_val]      

        tfidf_vectorizer = TfidfVectorizer(use_idf=True)
        X_entrenamiento_vectors_tfidf = tfidf_vectorizer.fit_transform(X_entrenamiento) 
        X_val_vectors_tfidf = tfidf_vectorizer.transform(X_val) 

        modelw = Vectorizador(w2v)
        X_entrenamiento_vectors_w2v = modelw.transformar(X_entrenamiento_tok)
        X_val_vectors_w2v = modelw.transformar(X_val_tok)

        lr_tfidf=LogisticRegression(solver = 'liblinear', C=10, penalty = 'l2')
        lr_tfidf.fit(X_entrenamiento_vectors_tfidf, y_entrenamiento)  

        y_predict = lr_tfidf.predict(X_val_vectors_tfidf)
        y_prob = lr_tfidf.predict_proba(X_val_vectors_tfidf)[:,1]
        
        print(classification_report(y_val,y_predict))
        print('Confusion Matrix:',confusion_matrix(y_val, y_predict))
        
        fpr, tpr, thresholds = roc_curve(y_val, y_prob)
        roc_auc = auc(fpr, tpr)
        print('AUC:', roc_auc)
        pickle.dump(tfidf_vectorizer, open("tfidf_vectorizer.pickle", "wb"))
        pickle.dump(lr_tfidf, open("lr_tfidf.pickle", "wb"))

        return roc_auc        

    def __predecir(self, dataframe):
        tfidf_vectorizer = pickle.load(open("tfidf_vectorizer.pickle", "rb"))
        lr_tfidf = pickle.load(open("lr_tfidf.pickle", "rb"))
        X_test=dataframe['clean_text']        
        X_vector = tfidf_vectorizer.transform(X_test) 
        y_predict = lr_tfidf.predict(X_vector)     
        y_prob = lr_tfidf.predict_proba(X_vector)[:,1]
        dataframe['predict_prob']= y_prob
        dataframe['target']= y_predict
        print(dataframe)
        print('PROBABILIDAD DEPRESION:')
        print(dataframe["predict_prob"].mean())
        final=dataframe[['id','target']].reset_index(drop=True)
        final.to_csv('submission.csv')

    def __obtenerDatosEntrenamiento(self):
        df_entrenamiento=pd.read_csv('train.csv')
        df_entrenamiento.head()
        
        df_entrenamiento['clean_text'] = df_entrenamiento['text'].apply(lambda x: self.__procesarTexto(x))
        
        df_entrenamiento['clean_text_tok']=[nltk.word_tokenize(i) for i in df_entrenamiento['clean_text']] 

        return df_entrenamiento
        
    def __concatenarTextPosts(self, dataframe):
        texto = ''
        
        for i in range(0, len(dataframe)):
            texto = texto + ' ' + dataframe.iloc[i]['clean_text']
        
        return texto 

    def __extraerPalabras(self, listapalabras):
        lista = list()

        for x in listapalabras:
            lista.extend(x.split())

        return lista       

    def analizar(self, listaPosts):
        dataframe = pd.DataFrame(listaPosts)         
        dataframe['clean_text'] = dataframe['text'].apply(lambda x: self.__procesarTexto(x)) 
        print('##### Posts sin limpiar ######')
        print(dataframe['text'])
        print('##### Posts limpios ######')
        print(dataframe['clean_text'])
        self.__predecir(dataframe)
        listaPalabrasLimpias = self.__extraerPalabras(dataframe['clean_text'].tolist())
        textoNube = self.__concatenarTextPosts(dataframe)
        resultadosJson = {}
        resultadosJson['listaTokenCounts'] = self.__tokenCountsToListObject(pd.DataFrame(listaPalabrasLimpias))        
        resultadosJson['relacionOntologia'] = self.__procesarOntologia(listaPalabrasLimpias, self.ontologiaObject)
        resultadosJson['nubePalabrasBase64'] = self.__generarNubePalabrasBase64(textoNube)
        resultadosJson['probabilidad'] = dataframe['predict_prob'].mean()
        print(resultadosJson)
        return jsonify(resultadosJson)
    

    def __generarNubePalabrasBase64(self, textoNube):
        s = io.BytesIO()
        wordcloud = WordCloud(width=2200, height=600,max_words=20).generate(textoNube) 
        
        plt.figure() 
        plt.imshow(wordcloud, interpolation="bilinear") 
        plt.axis("off") 
        plt.margins(x=0, y=0) 
        plt.savefig(s, format='png', bbox_inches="tight")        
        plt.close()

        return base64.b64encode(s.getvalue()).decode("utf-8").replace("\n", "")  
        
    def __tokenCountsToListObject(self, dataframe):        
        listaTokenCounts = list()

        for idx,name in enumerate(dataframe[0].value_counts().index.tolist()):
            listaTokenCounts.append({'token': name, 'count': int(dataframe[0].value_counts()[idx])})
        
        return listaTokenCounts
             
       
    def __procesarOntologia(self, listaPalabras, ontologiaJson):
        lista = list()
        
        for key in ontologiaJson:
            setPalabrasEstudiar = set(listaPalabras)
            categoriaOntologia = set(ontologiaJson[key])
            categoriaOntologia = setPalabrasEstudiar & categoriaOntologia
            
            if len(categoriaOntologia) > 0:
                lista.append({'categoria': key, 'valores':list(categoriaOntologia)})
        
        return lista  


    def __procesarTexto(self, texto):
        textoLimpio = self.__limpiarTexto(texto)
        textoLimpio = self.__eliminarPalabrasSinSentido(textoLimpio)
        textoTokenizado = self.nlp(textoLimpio)
        textoLemmatizado = ' '.join([palabra.lemma_ for palabra in textoTokenizado])
        return textoLemmatizado       

    # Metodo que procesa un texto y lo limpia
    def __limpiarTexto(self, texto):              
        textoLimpio = self.__convertirTextoAminuscula(texto)        
        textoLimpio = self.__eliminarLinks(textoLimpio)
        textoLimpio = self.__eliminarHashtagsMenciones(textoLimpio)
        textoLimpio = self.__eliminarSaltoLinea(textoLimpio)
        textoLimpio = self.__eliminarTildes(textoLimpio)
        textoLimpoo = self.__eliminarGuiones(textoLimpio)
        textoLimpio = self.__eliminarCaracteresNoAlfanumericos(textoLimpio)
        textoLimpio = self.__eliminarCaracteresRepetidos(textoLimpio)
        textoLimpio = self.__eliminarEspacioGrandes(textoLimpio)        
        return textoLimpio

    # Metodo que elimina las tildes de un texto dado.
    def __eliminarTildes(self, texto):
        eliminarTildes = dict.fromkeys(map(ord, u'\u0301\u0308'), None)
        return uni.normalize('NFKC', uni.normalize('NFKD', texto).translate(eliminarTildes))  

    # Metodo que elimina palabras de un texto
    def __eliminarPalabrasSinSentido(self, texto):
        textoLimpio = texto.split()
        textoLimpio = [w for w in textoLimpio if not w in self.listaPalabrasVacias]
        textoLimpio = " ".join(word for word in textoLimpio)
        return textoLimpio

    # Metodo que convierte todas las palabras de un texto en minusculas
    def __convertirTextoAminuscula(self, texto):
        return texto.lower() 

    # Metodo que elimina links de un texto
    def __eliminarLinks(self, texto):
        return re.sub(r'''(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))''', "", texto)
          
    # Metodo que elimina hashtag y menciones de un texto
    def __eliminarHashtagsMenciones(self, texto):
        textoLimpio = re.sub("@[A-Za-z0-9]+", "", texto)
        textoLimpio = re.sub("#[A-Za-z0-9]+", "", textoLimpio)
        return textoLimpio

    # Metodo que elimina caracteres no alfanumericos
    def __eliminarCaracteresNoAlfanumericos(self, texto):
        return re.sub("[^A-Za-z0-9]", " ", texto)

    # Metodo que elimina los espacios grandes
    def __eliminarEspacioGrandes(self, texto):
        return re.sub('\s{2,}', " ", texto)

    # Metodo que elimina saltos de linea
    def __eliminarSaltoLinea(self, texto):
        return re.sub("\n", " ", texto)

    # Metodo que elimina tres o mas caracteres repetidos
    def __eliminarCaracteresRepetidos(self, texto):
        return re.sub("(.)\\1{2,}", "\\1\\1", texto)

    # Metodo que elimina el guion entre palabras compuestas y las une entre si
    def __eliminarGuiones(self, texto):
        return re.sub("([A-Za-z]+)-([A-Za-z]+)", "\\1\\2", texto)