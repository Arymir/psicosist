from flask import Flask, request
from flask_restful import Resource, Api
from analizadorDepresion import AnalizadorDepresion
import json

app = Flask(__name__)
api = Api(app)

@app.route("/entrenamiento") #aquí definimos el primer path de la API: GET /
def hello(): #este método se llamará cuando el cliente haga el request
    return "Hello World!" #flask devolverá "Hello World, esto podría ser un string HTML o un string JSON.

class AnalisisDepresion(Resource):
    analizador = any

    def __init__(self):
        with open('ontologia.json') as json_file:
            ontologia = json.load(json_file) 
        self.analizador =  AnalizadorDepresion('spanish', ontologia);

    def get(self, id):     
        if id == 1:
            a = self.analizador.entrenarNaiveBayesTfIdf() 
        elif id == 2:
            a = self.analizador.entrenarLogicRegressionW2v()
        else:
            a = self.analizador.entrenarLogicRegressionTfIdf()

        return {'auc':  a}        
    
    def post(self):
        listaPosts = request.json['listaPost']
        return self.analizador.analizar(listaPosts)        

api.add_resource(AnalisisDepresion, '/analisisDepresion/<id>')  # Route_1

if __name__ == '__main__':
     app.run(host='0.0.0.0', port='9091')
