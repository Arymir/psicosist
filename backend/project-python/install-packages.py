import sys
import subprocess

# implement pip as a subprocess:
subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'flask'])
print('Libreria flask instalada')

subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'flask_restful'])
print('Libreria flask_restful instalada')

subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'pandas'])
print('Libreria pandas instalada')

subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'matplotlib'])
print('Libreria matplotlib instalada')

subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'sklearn'])
print('Libreria sklearn instalada')

subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'wordcloud'])
print('Libreria wordcloud instalada')

subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'gensim==3.8.3'])
print('Libreria gensim instalada')

subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'nltk'])
print('Libreria nltk instalada')