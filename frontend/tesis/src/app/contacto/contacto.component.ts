import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.css']
})
export class ContactoComponent implements OnInit {

  public listaEmails: string[] = ['arymiro@gmail.com','aortega069@e.uneg.edu.ve','kriptza@gmail.com']; 

  constructor() {}

  ngOnInit(): void {    
  }

}
