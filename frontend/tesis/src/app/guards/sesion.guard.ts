import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, Router } from '@angular/router';
import { LocalStorageService } from '../services/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class SesionGuard implements CanActivate {

  constructor(private localStorageService: LocalStorageService, private router: Router) { }

  
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.localStorageService.obtenerTokenFb()) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
  
  
}
