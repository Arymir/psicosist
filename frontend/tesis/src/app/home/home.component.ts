import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { LocalStorageService } from '../services/local-storage.service';
import { ConsumoApiRestService } from '../services/consumo-api-rest.service';
import { PostDto } from '../models/post-dto.model';
import { UserDto } from '../models/user-dto.model';
import { ResultadoDto } from '../models/resultado-dto.model';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  listaPost: Array<PostDto>;
  userDto: UserDto;
  resultadoDto: ResultadoDto;  

  constructor(    
    private localStorageService: LocalStorageService,
    private consumoApiRestService: ConsumoApiRestService) { }

  ngOnInit(): void {   
  }

  private obtenerPost(userId: string): void {
    this.consumoApiRestService.obtenerPosts(userId).subscribe(
      (response) => { 
        this.listaPost = response;
      },
      (error) => { 
        this.listaPost = [];
      });
  }

  private obtenerResultado(userId: string):void {
    this.consumoApiRestService.obtenerResultado(userId).subscribe(
      (response) => {         
        this.resultadoDto = response;
      },
      (error) => {}
    );    
  }

  public inicializarDatos(): void{
    this.userDto = this.localStorageService.obtenerDatosPerfil();    
  }

  public analizar():void{
    this.userDto = this.localStorageService.obtenerDatosPerfil();
    this.obtenerPost(this.userDto.id);
    this.obtenerResultado(this.userDto.id);
  }

  public descargarPDF():void {
    const DATA = document.getElementById('container-resultados');
    const doc = new jsPDF('p', 'pt', 'a4');
    const options = {
      background: 'white',
      scale: 1
    };
    html2canvas(DATA, options).then((canvas) => {

      const img = canvas.toDataURL('image/PNG');

      // Add image Canvas to PDF
      const bufferX = 15;
      const bufferY = 15;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      docResult.save(`${new Date().toISOString()}_tutorial.pdf`);
    });

  }
}
