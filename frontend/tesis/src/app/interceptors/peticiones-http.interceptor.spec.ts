import { TestBed } from '@angular/core/testing';

import { PeticionesHttpInterceptor } from './peticiones-http.interceptor';

describe('PeticionesHttpInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      PeticionesHttpInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: PeticionesHttpInterceptor = TestBed.inject(PeticionesHttpInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
