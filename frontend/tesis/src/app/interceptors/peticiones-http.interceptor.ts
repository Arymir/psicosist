import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators'
import { NgxSpinnerService } from "ngx-spinner";

@Injectable()
export class PeticionesHttpInterceptor implements HttpInterceptor {

  constructor(private spinner: NgxSpinnerService){
  }
  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    this.spinner.show();
    return next.handle(request)
      .pipe(catchError((err) => {
        this.spinner.hide();
        return err;
      }))
      .pipe(map<HttpEvent<any>, any>((evt: HttpEvent<any>) => {
        if (evt instanceof HttpResponse) {
          this.spinner.hide();
        }
        return evt;
      }));
  }
}

export const peticioneshttpInterceptor = [ {provide: HTTP_INTERCEPTORS, useClass: PeticionesHttpInterceptor, multi: true}];

