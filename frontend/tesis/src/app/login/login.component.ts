import { Component, OnInit, ViewChild } from '@angular/core';
import { ConsumoApiRestService } from '../services/consumo-api-rest.service';
import { LocalStorageService } from '../services/local-storage.service';
import { TokenDto } from '../models/token-dto.model';
import { SocialAuthService,  FacebookLoginProvider, SocialUser } from 'angularx-social-login';
import { UserDto } from '../models/user-dto.model';
import { Router } from '@angular/router';
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';

interface Alert {
  type: string;
  message: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private _success = new Subject<string>();

  socialUser: SocialUser;
  userLogged: SocialUser;
  isLogged: boolean; 
  boAutoriza: boolean;
  staticAlertClosed = true;
  
  @ViewChild('staticAlert', {static: false}) staticAlert: NgbAlert;

  constructor(     
    private socialAuthService: SocialAuthService,  
    private consumoApiRestService: ConsumoApiRestService,
    private localStorageService: LocalStorageService, 
    private router: Router
  ) {
    this.boAutoriza = false;
  }
  

  ngOnInit(): void {
    this.socialAuthService.authState.subscribe((user: SocialUser) => {
      if (!user) {
        this.localStorageService.limpiarStorage();
      }
    });
  }

  signInWithFB(): void {
    if(this.boAutoriza){
      this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then(
        data => {        
          let tokenDto = new TokenDto(data.authToken);
          this.localStorageService.almacenarTokenFb(tokenDto);
          this.consumoApiRestService.login(tokenDto).subscribe(
            (res) => { 
              let userDto = new UserDto(res.id, res.email, res.name, res.gender, res.avatarUrl);
              this.localStorageService.almacenarDatosPerfil(userDto);
              this.router.navigate(['/']);
            }
          );        
        }
      ).catch(
        err => {
          console.log(err);
        }
      );
    }else{
      this.staticAlertClosed = false;
      setTimeout(() => this.staticAlert.close(), 2000);
    }    
  }

 
}
