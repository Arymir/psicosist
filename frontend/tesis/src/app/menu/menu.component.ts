import { Component, OnInit } from '@angular/core';
import { UserDto } from '../models/user-dto.model';
import { Router } from '@angular/router';
import { LocalStorageService } from '../services/local-storage.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  isLogged: boolean;
  userDto: UserDto;

  constructor(private localStorageService: LocalStorageService, private router: Router) { 
    this.isLogged = false;
  }

  ngOnInit(): void {
    this.userDto = this.localStorageService.obtenerDatosPerfil();
  }

  signOut(): void {
    this.localStorageService.limpiarStorage();
    this.router.navigate(['/login']);
  }
}
