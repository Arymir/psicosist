export class RelacionOntologiaDto {
    categoria:string;
    listaValores: Array<string>;

    constructor(categoria:string, listaValores: Array<string>){
        this.categoria = categoria;
        this.listaValores = listaValores;
    }
}
