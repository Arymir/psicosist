import { TokenCountDto } from './token-count-dto.model';
import { RelacionOntologiaDto } from './relacion-ontologia-dto.model';

export class ResultadoDto {
    listaTokenCounts: Array<TokenCountDto>;
    relacionOntologia: Array<RelacionOntologiaDto>;
    nubePalabrasBase64: string;
    presenciaSintomas: string;
    nivelDepresion: string;
    recomendacion: string;
    probabilidad: number;
    fechaGeneracion: string;

    constructor(listaTokenCounts: Array<TokenCountDto>, 
                relacionOntologia: Array<RelacionOntologiaDto>, 
                nubePalabrasBase64:string,
                presenciaSintomas: string,
                nivelDepresion: string,
                recomendacion: string,
                probabilidad: number,
                fechaGeneracion: string
    ){
        this.listaTokenCounts = listaTokenCounts;
        this.relacionOntologia = relacionOntologia;
        this.nubePalabrasBase64 = nubePalabrasBase64;
        this.presenciaSintomas = presenciaSintomas;
        this.nivelDepresion = nivelDepresion;
        this.recomendacion = recomendacion;
        this.probabilidad = probabilidad;
        this.fechaGeneracion = fechaGeneracion;
    }
}
