export class UserDto {
    id: string;
    email: string;
    name: string;
    gender: string;
    avatarUrl: string;

    constructor(id: string, email: string, name: string, gender: string, avatarUrl:string){
        this.id = id;
        this.email = email;
        this.name = name;
        this.gender = gender;
        this.avatarUrl = avatarUrl;
    }
}
