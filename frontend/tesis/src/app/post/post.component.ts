import { Component, OnInit, Input } from '@angular/core';
import { PostDto } from '../models/post-dto.model';
import { UserDto } from '../models/user-dto.model';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  @Input('post')
  postDto: PostDto;

  @Input('user')
  userDto: UserDto;

  constructor() { }

  ngOnInit(): void {
  }

}
