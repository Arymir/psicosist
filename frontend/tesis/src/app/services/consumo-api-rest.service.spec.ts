import { TestBed } from '@angular/core/testing';

import { ConsumoApiRestService } from './consumo-api-rest.service';

describe('ConsumoApiRestService', () => {
  let service: ConsumoApiRestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConsumoApiRestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
