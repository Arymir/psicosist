import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenDto } from '../models/token-dto.model';
import { PostDto } from '../models/post-dto.model';
import { UserDto } from '../models/user-dto.model';
import { Observable } from 'rxjs';
import { ResultadoDto } from '../models/resultado-dto.model';

const cabecera = {headers: new HttpHeaders({'Content-Type' : 'application/json'})};

@Injectable({
  providedIn: 'root'
})
export class ConsumoApiRestService {

  BASE_URL = 'http://tesisvm.eastasia.cloudapp.azure.com:9090';

  constructor(private httpClient: HttpClient) { }

  public login(tokenDto: TokenDto): Observable<UserDto> {
    let url = this.BASE_URL  + '/login';
    return this.httpClient.post<UserDto>(url, tokenDto, cabecera);
  }

  public obtenerPosts(id: string): Observable<Array<PostDto>>{
    let url = this.BASE_URL  + '/post/findByUser/' + id;
    return this.httpClient.get<Array<PostDto>>(url, cabecera);
  }

  public obtenerResultado(idUser: string): Observable<ResultadoDto>{
    let url = this.BASE_URL  + '/post/analisis/' + idUser;
    return this.httpClient.get<ResultadoDto>(url, cabecera);
  }
}
