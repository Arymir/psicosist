import { Injectable } from '@angular/core';
import { UserDto } from '../models/user-dto.model';
import { TokenDto } from '../models/token-dto.model';

const FB_TOKEN_KEY = 'fbToken';
const PERFIL_KEY = 'perfilToken';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  almacenarTokenFb(token: TokenDto): void{
    sessionStorage.removeItem(FB_TOKEN_KEY);
    sessionStorage.setItem(FB_TOKEN_KEY, token.value);
  }

  obtenerTokenFb(): TokenDto | null {
    let stToken= sessionStorage.getItem(FB_TOKEN_KEY);
    
    if(null !== stToken){
      return new TokenDto(stToken);
    }

    return null;
  }

  almacenarDatosPerfil(userDto: UserDto){
    sessionStorage.removeItem(PERFIL_KEY);
    sessionStorage.setItem(PERFIL_KEY, JSON.stringify(userDto));
  }

  obtenerDatosPerfil(): UserDto {    
    let stDatos = sessionStorage.getItem(PERFIL_KEY);
    let userDto: UserDto = JSON.parse(null !== stDatos? stDatos: '');
    return userDto;      
  }

  limpiarStorage(): void {
    sessionStorage.clear();
  }
  
}
